allure-pytest==2.9.43
pytest==6.2.5
pytest-xdist==2.4.0
requests==2.25.1
selenium==3.141.0
Pillow==8.3.2
