import pytest
from selenium import webdriver

@pytest.fixture(scope="session")
def browser():
    capabilities = {
        "browserName": "chrome",
        "browserVersion": "108.0",
        "selenoid:options": {
            "enableVNC": True,
            "enableVideo": True
        }
    }

    driver = webdriver.Remote(
        command_executor="http://127.0.0.1:4444/wd/hub",
        desired_capabilities=capabilities)
    yield driver
    driver.quit()
