from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import allure

class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "http://the-internet.herokuapp.com/"

    def find_element(self, locator,time=10):
        return WebDriverWait(self.driver,time).until(EC.presence_of_element_located(locator),
                                                      message=f"Can't find element by locator {locator}")

    def find_elements(self, locator,time=10):
        return WebDriverWait(self.driver,time).until(EC.presence_of_all_elements_located(locator),
                                                      message=f"Can't find elements by locator {locator}")

    def open(self):
        return self.driver.get(self.base_url)

    def basic_auth(self, user, pswd):
        with allure.step("set login and password for auth: " + user + " / " + pswd ):
          return self.driver.get("http://" + user + ":" + pswd + "@the-internet.herokuapp.com/basic_auth")

    def return_back(self):
        with allure.step("click back"):
            self.driver.execute_script("window.history.go(-1)")

    def scroll_down(self):
        with allure.step("swipe down"):
            self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
