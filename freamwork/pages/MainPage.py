from freamwork.base.BaseApp import BasePage
from selenium.webdriver.common.by import By
import allure


class MainPageLocators:
    # TODO: find more stable locator
    FILE_UPLOAD = (By.XPATH, '//*[@id="content"]/ul/li[18]/a')
    FILE_DOWNLOAD = (By.XPATH, '//*[@id="content"]/ul/li[17]/a')
    HOVERS = (By.XPATH, '//*[@id="content"]/ul/li[25]/a')

class MainPage(BasePage):
    def click_upload_file(self):
        with allure.step("click file upload link"):
            self.find_element(MainPageLocators.FILE_UPLOAD).click()

    def click_download_file(self):
        with allure.step("click file download link"):
            self.find_element(MainPageLocators.FILE_DOWNLOAD).click()

    def click_hovers(self):
        with allure.step("click hovers link"):
            self.find_element(MainPageLocators.HOVERS).click()
