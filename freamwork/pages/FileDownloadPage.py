from freamwork.base.BaseApp import BasePage
from selenium.webdriver.common.by import By
import allure



class FileDownloadLocators:
    # TODO: add new locator
    DOWNLOAD_FILE = (By.XPATH, '//*[contains(text(),\'some-file.txt\')]')

class FileDownloadPage(BasePage):
    def download_file(self):
        with allure.step("file download"):
          self.find_element(FileDownloadLocators.DOWNLOAD_FILE).click()


