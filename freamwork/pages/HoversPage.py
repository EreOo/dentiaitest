from freamwork.base.BaseApp import BasePage
from selenium.webdriver.common.by import By
import allure


class HoversPageLocators:
    # TODO: find more stable locator
    FIRST_AVATAR = (By.XPATH, '//*[@id="content"]/div/div[1]/img')
    FIRST_PROFILE_LINK = (By.XPATH, '//*[@id="content"]/div/div[1]/div/a')
    # TODO: new page for profile logic
    NOT_FOUND_TEXT = (By.XPATH, '/html/body/h1')

class HoversPage(BasePage):
    def view_profile(self):
        with allure.step("click view profile"):
           self.find_element(HoversPageLocators.FIRST_AVATAR).click()
           self.find_element(HoversPageLocators.FIRST_PROFILE_LINK).click()

    def check_profile(self):
        with allure.step("check profile"):
           assert 'Not Found' in self.find_element(HoversPageLocators.NOT_FOUND_TEXT).text