from freamwork.base.BaseApp import BasePage
from selenium.webdriver.common.by import By
import allure



class FileUploadLocators:
    CHOOSE_FILE = (By.XPATH, '//*[@id="file-upload"]')
    UPLOAD_FILE = (By.XPATH, '//*[@id="file-submit"]')
    SUCCESS_UPLOAD = (By.XPATH, '//*[@id="content"]/div/h3')

class FileUploadPage(BasePage):
    def choose_file(self):
        with allure.step("set file to upload"):
          fileUpload = self.find_element(FileUploadLocators.CHOOSE_FILE)
          # TODO: not best solution. It's hardcode now, but it's for exemple :p
          fileUpload.send_keys("/home/selenium/Downloads/some-file.txt")

    def upload_button_click(self):
        with allure.step("click upload_button"):
            self.find_element(FileUploadLocators.UPLOAD_FILE).click()

    def check_success_upload(self):
        with allure.step("check success upload"):
            assert "File Uploaded!" in self.find_element(FileUploadLocators.SUCCESS_UPLOAD).text
