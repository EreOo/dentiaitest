from freamwork.base.BaseApp import BasePage
from selenium.webdriver.common.by import By
import allure


class AuthPageLocators:
    # TODO: find more stable locator
    SUCCESS_AUTH_TEXT = (By.XPATH, '//*[@id="content"]/div/p')

class AuthPage(BasePage):
    def check_success_auth(self):
        with allure.step("check success auth"):
            assert 'Congratulations! You must have the proper credentials.' in self.find_element(AuthPageLocators.SUCCESS_AUTH_TEXT).text


