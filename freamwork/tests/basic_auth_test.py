from freamwork.pages.AuthPage import AuthPage

import allure

@allure.feature('Test check basic auth.')
def test_basic_auth(browser):
    authPage = AuthPage(browser)
    authPage.basic_auth('admin', 'admin')
    authPage.check_success_auth()

@allure.feature('Test check FAIL basic auth.')
def test_fail_basic_auth(browser):
    authPage = AuthPage(browser)
    authPage.basic_auth('wrong', 'password')
    authPage.check_success_auth()