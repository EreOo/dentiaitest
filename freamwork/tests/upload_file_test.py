from freamwork.pages.MainPage import MainPage
from freamwork.pages.FileUploadPage import FileUploadPage
from freamwork.pages.FileDownloadPage import FileDownloadPage
import allure

@allure.feature('Test check upload file txt format.')
def test_upload_file(browser):
    download_file(browser)
    mainPage = MainPage(browser)
    mainPage.open()
    mainPage.click_upload_file()
    fileUploadPage = FileUploadPage(browser)
    fileUploadPage.choose_file()
    fileUploadPage.upload_button_click()
    fileUploadPage.check_success_upload()


# Precondition:
def download_file(browser):
    with allure.step("Prepare file for upload"):
        mainPage = MainPage(browser)
        mainPage.open()
        mainPage.click_download_file()
        fileDownloadPage = FileDownloadPage(browser)
        fileDownloadPage.download_file()
        fileDownloadPage.return_back()