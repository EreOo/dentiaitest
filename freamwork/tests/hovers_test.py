from freamwork.pages.MainPage import MainPage
from freamwork.pages.HoversPage import HoversPage

import allure

@allure.feature('Test check hovers and profile.')
def test_hovers(browser):
    mainPage = MainPage(browser)
    mainPage.open()
    mainPage.scroll_down()
    mainPage.click_hovers()
    hoversPage = HoversPage(browser)
    hoversPage.view_profile()
    hoversPage.check_profile()